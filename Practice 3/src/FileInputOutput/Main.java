package FileInputOutput;

import java.io.*;

public class Main {

	public static void main(String[] args) {
		
//		Write to:
		
//		try {
//			BufferedWriter bw = new BufferedWriter(new FileWriter("src/FileInputOutput/resource/output.txt"));
//			bw.write("Karen\n");
//			bw.write("Chad\n");
//			bw.write("Becky\n");
//			bw.close();
//
//		}
//		catch (IOException exc) {
//			System.out.println(exc.getMessage());
//		}
		
//		Read from: 
		
//		try {
//			BufferedReader br = new BufferedReader(new FileReader("src/FileInputOutput/resource/output.txt"));
//			
//			String s;
//			while((s = br.readLine()) != null) {
//				System.out.println(s);
//			}
//			br.close();
//		}
//		catch(IOException exc) {
//			return;
//		}
		
//		Copy/Paste:
		
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("src/FileInputOutput/resource/output-copy.txt"));
			BufferedReader br = new BufferedReader(new FileReader("src/FileInputOutput/resource/output.txt"));
			
			String s;
			while((s = br.readLine()) != null) {
				bw.write(s + "\n");
			}
			br.close();
			bw.close();
		}
		catch(IOException exc) {
			return;
		}
		
//		I can also go with the teacher's example
		
	}

}
