package registration;

import java.io.IOException;

public class RegistrationEveningStudent implements IRegistrationService {

	@Override
	public void registerStudent(Student student) {
		System.out.println("this is evening " + student);
		
		try {
			FileUtils.fileWriter("evening", student);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

}
