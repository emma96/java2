package WriteToFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		List<String> addList = new ArrayList<String>();

		Address add1 = new Address(1239, "Oiseaux", "Laval", "Qc", "H2R 1P9");
		Address add2 = new Address(2324, "Langelier", "Montreal", "Qc", "H5M 3W4");
		Address add3 = new Address(4547, "Lucerne", "Laval", "Qc", "H2R 1P9");
		Address add4 = new Address(6768, "Repentigny", "Montreal", "Qc", "H2R 1P9");
		Address add5 = new Address(6523, "Bergeronette", "Laval", "Qc", "H2R 1P9");

		addList.add(add1.toString());
		addList.add(add2.toString());
		addList.add(add3.toString());
		addList.add(add4.toString());
		addList.add(add5.toString());

		writeToFile(addList);
		System.out.println("writeToFile() method completed");
		
		System.out.println();
		
		readFromFile();

	}

	public static void writeToFile(List<String> addList) {

		try {

			BufferedWriter bw = new BufferedWriter(new FileWriter("src/WriteToFile/resource/output.txt"));
			addList.forEach(add -> {
				try {
					bw.write(add + "\n");
				} catch (IOException e) {
					System.out.println(e.getMessage());
				}
			});
			bw.close();

		} catch (IOException exc) {
			System.out.println(exc.getMessage());
		}

	}

	public static void readFromFile() {
		try {
			BufferedReader br = new BufferedReader(new FileReader("src/WriteToFile/resource/output.txt"));

			String s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}
			br.close();
		} catch (IOException exc) {
			System.out.println(exc.getMessage());
		}
	}

}
