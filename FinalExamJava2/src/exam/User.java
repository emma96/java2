package exam;

public class User {
	
	private String firstName;
	
	private String lastName;
	
	private Address address;

	//Constructor()
	public User(String firstName, String lastName, Address address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
	}
	
	//Getters and Setters
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	//toString()
	@Override
	public String toString() {
		return "User [firstName= " + firstName + ", lastName= " + lastName + ", address= " + address + "]";
	}

}
