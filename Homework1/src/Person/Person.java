package Person;

public class Person {
	
	private String firstName;
	
	//Setter for firstName
	public void setFirstName(String firstName) {
		
		this.firstName = firstName;
		
	}
	
	//Getter for firstName
	public String getFirstName() {
		
		return firstName;
		
	}
	
	
	private String lastName;
	
	//Setter for lastName
	public void setLastName(String lastName) {
		
		this.lastName = lastName;
		
	}
	
	//Getter for lastName
 	public String getLastName() {
		
		return lastName;
		
	}
	
 	//Getter for fullName
 	public String getFullName() {
 		
 		String fullName = this.firstName + " " + this.lastName;
 		
 		if (this.firstName.isEmpty()) {
 			
 			fullName = this.lastName;
 			
 		}
 		
 		if (this.lastName.isEmpty()) {
 			
 			fullName = this.firstName;
 			
 		}
 		
 		if (this.firstName.isEmpty() && this.lastName.isEmpty()) {
 			
 			fullName = "";
 			
 		}
 		
 		return fullName;
 		
 	}
 	
 	
	private int age;
	
	//Setter for age
	public void setAge(int age) {
		
		this.age = age;
		
		if(age < 0 || age > 100) {
			
			this.age = 0;
			
		}
		
		
	}
	
	//Getter for age
	public int getAge() {
		
		return age;
		
	}
	
	//isTeen boolean
	public boolean isTeen() {
		
//		if(age > 12 && age < 20) {
//			
//			return true;
//			
//		} else {
//			
//			return false;
//			
//		}
		
		return (age > 12 && age < 20);
		
	}
}
