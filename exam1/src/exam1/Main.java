package exam1;

public class Main {

	public static void main(String[] args) {
		
		// Calculator
		Carpet carpet = new Carpet(3.5);
		
		Floor floor = new Floor(2.75, 4.0);
		
		Calculator calculator = new Calculator(floor, carpet);
		
		System.out.println("total = " + calculator.getTotalCost());
		
		carpet = new Carpet(1.5);
		
		floor = new Floor(5.4, 4.5);
		
		calculator = new Calculator(floor, carpet);
		
		System.out.println("total = " + calculator.getTotalCost());
		
		System.out.println();
		
		//Banks
		
		BankA bankA = new BankA();
		
		BankB bankB = new BankB();
		
		BankC bankC = new BankC();
		
		System.out.printf("BankA balance = %d \nBankB balance = %d \nBankC balance = %d\n", bankA.getBalance(), bankB.getBalance(), bankC.getBalance());

		System.out.println();
		
		//PC
		
		Dimension dimension = new Dimension(20, 20, 5);
		
		Case theCase = new Case("220B", "Dell", "240", dimension);
		
		Monitor theMonitor = new Monitor("27inch Beast", "Acer", 27, new Resolution(2540, 1440));
		
		Motherboard theMotherboard = new Motherboard("BJ-200", "Asus", 4, 6, "v2.44");
		
		PC thePC = new PC(theMonitor, theCase, theMotherboard);
		
		thePC.getTheCase().pressPowerButton();
		
	}

}
