package exercice1;

public class Students implements Comparable<Students>{
	
	private double score;
	
	private String name;

	public Students(double score, String name) {
		this.score = score;
		this.name = name;
	}

	public double getScore() {
		return score;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Students [Score= " + score + ", name= " + name + "]";
	}

	@Override
	public int compareTo(Students o) {
		
//		return this.name.compareTo(o.name); Sorting them by name
		if(this.score == o.score) {
			
			return 0;
			
		} else if(this.score < o.score) {
			
			return 1;
			
		} else {
			
			return -1;
			
		}
		
	}
	
	

}
