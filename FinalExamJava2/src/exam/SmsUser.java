package exam;

import java.util.regex.*;

public class SmsUser extends User {
	
	private String phoneNumber;

	//Constructor()
	public SmsUser(String firstName, String lastName, Address address, String phoneNumber) {
		super(firstName, lastName, address);
		setPhoneNumber(phoneNumber);
	}

	//Getter and Setter
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	//!!!! Come back to this one if time NOT GOOD
//	public void setPhoneNumber(String phoneNumber) throws IllegalArgumentException{
//		if(phoneNumber.matches("[a-z]")) {
//			throw new IllegalArgumentException();
//
//		} else {
//			this.phoneNumber = phoneNumber;
//		}
//	}
	
	//Teacher's solution:

	public void setPhoneNumber(String phoneNumber) throws IllegalArgumentException{
		if(Pattern.matches("\\d+", phoneNumber)) {
			this.phoneNumber = phoneNumber;
		}else {
			throw new IllegalArgumentException("Phone number needs to have only numbers");
		}
	}

	//toString()
	@Override
	public String toString() {
		return super.toString() + "SmsUser [phoneNumber=" + phoneNumber + "]";
	}
	
}
