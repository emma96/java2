import java.util.Comparator;

public class LocalDateComparator implements Comparator<User>{

	@Override
	public int compare(User o1, User o2) {
		return o1.getBirthdate().compareTo(o2.getBirthdate());
	}

}
