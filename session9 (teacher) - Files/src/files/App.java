package files;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class App {

	public static void main(String[] args) {
		try {
			//write methods
			System.out.println("Writing files started");

			bufferedWriterExample();
			fileWriterExample();
			fileOutputStreamExample();
			dataOutputStreamExample();
			filesWriteExample();
			
			System.out.println("");
			System.out.println("Reading files started");
			System.out.println("---------------");
			System.out.println("");

			
			//read methods
			System.out.println("bufferedReaderExample started");
			bufferedReaderExample();
			System.out.println("bufferedReaderExample ended");
			
			System.out.println("---------------");

			System.out.println("FileReaderExample started");
			fileReaderExample();
			System.out.println("FileReaderExample ended");

			System.out.println("---------------");

			System.out.println("fileInputStreamExample started");
			fileInputStreamExample();
			System.out.println("fileInputStreamExample ended");
			
			System.out.println("---------------");

			System.out.println("filesReadExample started");
			filesReadExample();
			System.out.println("filesReadExample ended");

			
		} catch (IOException e) {
			System.out.println("an exception happened");
		}

	}
	
	
	private static void bufferedWriterExample() throws IOException {
		// bufferedWriter
		String str1 = "My sentence1";
		String str2 = "My sentence2";

		try (BufferedWriter writer = new BufferedWriter(new FileWriter("src/resource/testfile1"))) 
		{
			writer.write(str1);
			writer.write("\n");
			writer.write(str2);
		} catch (IOException e) {
			throw e;
		}
	
		//since I used try with resource feature, I don't need to close the stream !!!
			
		System.out.println("bufferedWriterExample is done");
	}
	
	
	//clean and good idea!!!
	private static void bufferedReaderExample() throws IOException {
		try(BufferedReader reader = new  BufferedReader(new FileReader("src/resource/testfile1"))){
			Stream<String> lines = reader.lines();
			lines.forEach(line -> System.out.println(line));
		} catch (IOException e) {
			throw e;
		}
	}
	
	
	//it is good if you have text , go with this approach!!!
	private static void fileWriterExample() throws IOException {
		try(FileWriter fileWriter = new FileWriter("src/resource/testfile2")){
			PrintWriter printWriter = new PrintWriter(fileWriter);
		    printWriter.printf("Product name is %s and its price is %d $", "iPhone", 1000);
		    printWriter.println("");
			printWriter.println("this is the example for printwriter");
			printWriter.print("this is the second line");
		}
		catch(IOException exc) {
			throw exc;
		}
		
		System.out.println("printWriterExample is done");
	}
	
	
	//not a good idea!!!
	private static void fileReaderExample() throws IOException {
		try(FileReader reader = new FileReader("src/resource/testfile2")){
			int data = reader.read();
			//this code is error prone
			while(data != -1) {
				System.out.print((char)data);
				data = reader.read();
			}
			System.out.println("");
		} catch (IOException exc) {
			throw exc;
		}
	}
	
	
	private static void fileOutputStreamExample() throws IOException {
		String str = "this is A string that I need to convert into byte !!!!";
		//base64 hashed !!! encryption
		//the password needs to be decrypted 
		//cibersecurity 
		try(FileOutputStream outputStream = new FileOutputStream("src/resource/testfile3")){
			byte[] strToByte = str.getBytes();
			outputStream.write(strToByte);
		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}
		
		
		System.out.println("fileOutputStreamExample is done");
		
	}
	
	//this is not good. while loop!!!
	private static void fileInputStreamExample() throws IOException {
		
		try(FileInputStream inputStream = new FileInputStream("src/resource/testfile3")){
			int data = inputStream.read();
			//this code is error prone
			while(data != -1) {
				System.out.print((char)data);
				data = inputStream.read();
			}
			System.out.println("");
		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}
		
		
	}
	
	private static void dataOutputStreamExample() throws IOException {
		String str = "my sentence";
		try(FileOutputStream fos = new FileOutputStream("src/resource/testfile4");){
			DataOutputStream outputStream = new DataOutputStream(new BufferedOutputStream(fos));
			outputStream.writeUTF(str);
		} catch (IOException e) {
			throw e;
		}
				
		System.out.println("dataOutputStreamExample is done");

	}
	
	
	//Java 7 approaches... gooooood approaches
	private static void filesWriteExample() throws IOException {
		String str = "my test \n ttttt";
		Path path = Paths.get("src/resource/testfile5");
		byte[] strToBytes = str.getBytes();
		Files.write(path, strToBytes);
		System.out.println("filesExample is done");
	}
	
	private static void filesReadExample() throws IOException {
		Path path = Paths.get("src/resource/testfile5");
		List<String> lines = Files.readAllLines(path);
		lines.forEach(line -> System.out.println(line));
	}
	
	
	
	
	
	
	
	
}
