package homework;

public class Company {

	public static void main(String[] args) {
		
		Payroll pay = new Payroll(new ComissionEmployee("Bob", "Bob123", 15, 20), new HourlyEmployee("Sam", "Sam1234", 25, 7), new SalariedEmployee("Alex", "Alex12", 30));
		
		for(int i = 0; i < pay.employees.length; i++) {
			
			System.out.println(pay.employees[i].toString());
			
		}
		
		System.out.println();
		
		pay.paySalary();
		
		System.out.println();
		
		//Q.4:
		
		Computer computer1 = new Computer();
		
		//Upcasting
		
		Machine machine1 = computer1;
		
		machine1.start();
		//machine2.playGames(); ERROR
		
		System.out.println();
		
		//Downcasting
		
		Machine machine2 = new Computer();
		
		Computer computer2 = (Computer)machine2;
		
		computer2.start();
		computer2.playGames();
		
		//I have trouble understanding downcasting, will need to review.
		
	}

}
