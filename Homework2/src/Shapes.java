
abstract class Shapes {
	
	public abstract double getArea(double length);
	
	public abstract double getPerimeter(double length);
	
}
