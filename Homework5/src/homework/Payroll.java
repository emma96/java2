package homework;

public class Payroll {
	
	protected Employee[] employees = new Employee[3];
	
	Payroll(ComissionEmployee emp1, HourlyEmployee emp2, SalariedEmployee emp3) {
		
		this.employees[0] = emp1;
		
		this.employees[1] = emp2;
		
		this.employees[2] = emp3;
		
		
	}
	
	public void paySalary() {
		
		for(int i = 0; i < employees.length; i++) {
			
			System.out.println(employees[i].name + "'s salary is " + employees[i].salary() + "$");
			
		}
		
	}

}
