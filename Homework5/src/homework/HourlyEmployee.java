package homework;

public class HourlyEmployee extends Employee{

	protected double wage;
	
	protected double hours;
	
	HourlyEmployee(String name, String ssn, double wage, double hours) {
		super(name, ssn);
		
		this.wage = wage;
		
		this.hours = hours;
	}

	@Override
	public double salary() {
		return this.wage * this.hours;
	}

	@Override
	public String toString() {
		return "HourlyEmployee [wage=" + wage + ", hours=" + hours + ", name= " + name + ", ssn= " + ssn + "]";
	}

}
