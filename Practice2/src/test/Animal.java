package test;

public class Animal {
	
	protected String sound;
	
	Animal(String snd){
		
		this.sound = snd;
		
	}
	
	public String getSound() {
		
		return sound;
		
	}
	
	public void makeSound() {
		
		System.out.println("This is my sound : " + sound);
		
	}

}
