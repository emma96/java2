package HashSet;

import java.util.HashSet;

public class Main {

	public static void main(String[] args) {
		
		HashSet<String> cars = new HashSet<String>();
	    cars.add("Volvo");
	    cars.add("BMW");
	    cars.add("Ford");
	    cars.add("BMW");
	    cars.add("Mazda");
	    System.out.println(cars);
	    
	    System.out.println();
	    
	    //Note: In the example above, even though BMW is added twice it only appears once in the set because every item in a set has to be unique.

	    //remove()
	    
	    cars.remove("Volvo");
	    System.out.println(cars);
	    
	    System.out.println();
	    
	    //clear()
	    
	    cars.clear();
	    System.out.println(cars);
	    
	    System.out.println();
	    
	    //size()
	    
	    System.out.println(cars.size());
	    
	    System.out.println();
	    
	    //Loop through the items of an HashSet with a for-each loop:  

	    cars.add("Volvo");
	    cars.add("BMW");
	    cars.add("Ford");
	    cars.add("BMW");
	    cars.add("Mazda");
	    
	    for (String i : cars) {
	    	  System.out.println(i);
	    	}
	    
	    System.out.println();
	    
	    //Use a HashSet that stores Integer objects:

	    // Create a HashSet object called numbers
	    HashSet<Integer> numbers = new HashSet<Integer>();

	    // Add values to the set
	    numbers.add(4);
	    numbers.add(7);
	    numbers.add(8);

	    // Show which numbers between 1 and 10 are in the set
	    for(int i = 1; i <= 10; i++) {
	      if(numbers.contains(i)) {
	        System.out.println(i + " was found in the set.");
	      } else {
	        System.out.println(i + " was not found in the set.");
	      }
	    }
	   
	    
	}

}
