import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		User user1 = new User(123, "Sponge Bob", LocalDate.of(1980, 1, 1));
		User user2 = new User(456, "Frosty the Snowman", LocalDate.of(1950, 12, 25));
		User user3 = new User(789, "Iron Man", LocalDate.of(2000, 6, 16));
		User user4 = new User(198, "Wonder Woman", LocalDate.of(1990, 8, 21));
		User user5 = new User(576, "Super Man", LocalDate.of(2001, 4, 3));
		
		List<User> userList = new ArrayList<User>();
		
		userList.add(user1);
		userList.add(user2);
		userList.add(user3);
		userList.add(user4);
		userList.add(user5);
		
		userList.forEach(usr -> System.out.println(usr));
		
		//I don't understand the part of the assignment where it says :
		//Create a method name it as sortListBytype(int type) - based on the type sort them by id, by name and birthdate
		
		System.out.println();
		
		Collections.sort(userList, new NameComparator());
		System.out.println("After sorting by name: ");
		userList.forEach(usr -> System.out.println(usr.toString()));
		
		System.out.println();
		
		Collections.sort(userList, new IdComparator());
		System.out.println("After sorting by ID: ");
		userList.forEach(usr -> System.out.println(usr.toString()));
		
		System.out.println();
		
		Collections.sort(userList, new LocalDateComparator());
		System.out.println("After sorting by birthdate (oldest to youngest): ");
		userList.forEach(usr -> System.out.println(usr.toString()));
		
	}

}
