package WriteToFile;

public class Address {
	
	private int buildingNo;
	
	private String streetName;
	
	private String cityName;
	
	private String provinceName;
	
	private String postalCode;

	public Address(int buildingNo, String streetName, String cityName, String provinceName, String postalCode) {
		super();
		this.buildingNo = buildingNo;
		this.streetName = streetName;
		this.cityName = cityName;
		this.provinceName = provinceName;
		this.postalCode = postalCode;
	}

	public int getBuildingNo() {
		return buildingNo;
	}

	public String getStreetName() {
		return streetName;
	}

	public String getCityName() {
		return cityName;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public String getPostalCode() {
		return postalCode;
	}

	@Override
	public String toString() {
		return "Address [buildingNo=" + buildingNo + ", streetName=" + streetName + ", cityName=" + cityName
				+ ", provinceName=" + provinceName + ", postalCode=" + postalCode + "]";
	}

}
