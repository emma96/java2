package homework3;

public class Main {

	public static void main(String[] args) {
		
		Rectangle rectangle = new Rectangle(5, 10);
		
		System.out.printf("Rectangle width = %.1f\n", rectangle.getWidth());
		System.out.printf("Rectangle length = %.1f\n", rectangle.getLength());
		System.out.printf("Rectangle area = %.1f\n", rectangle.getArea());
		
		Cuboid cuboid = new Cuboid(5, 10, 5);
		
		System.out.printf("Cuboid width = %.1f\n", cuboid.getWidth());
		System.out.printf("Cuboid length = %.1f\n", cuboid.getLength());
		System.out.printf("Cuboid area = %.1f\n", cuboid.getArea());
		System.out.printf("Cuboid height = %.1f\n", cuboid.getHeight());
		System.out.printf("Cuboid volume = %.1f\n", cuboid.getVolume());

	}

}
