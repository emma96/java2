package Generics;

public class Main {

	public static void main(String[] args) {
		
//		MyClass<Integer> obj = new MyClass(10);    (if the class only had one <T>)
//		MyClass<Double> obj2 = new MyClass(20.0);
		
		MyClass<Integer, Double> obj3 = new MyClass(10, 20.0);
		
//		obj.showType();
//		obj2.showType();
		
		obj3.showType();
		
		System.out.println();
		
		NumericFns<Integer> iOb = new NumericFns<>(4);
		System.out.println(iOb.square());

	}

}
