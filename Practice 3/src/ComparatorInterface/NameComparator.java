package ComparatorInterface;

import java.util.Comparator;

public class NameComparator implements Comparator<Student>{

	@Override
	public int compare(Student o1, Student o2) {
		return o1.getName().compareTo(o2.getName());
	}

//	@Override
//	public int compare(Object o1, Object o2) {
//		Student std1 = (Student)o1;
//		Student std2 = (Student)o2;
//	}
//	This happens if I don't provide the Generic type <Student> after the Comparator interface and it's not that good because
//	I then need to cast the object to a student and it can become complicated for nothing
	
}
