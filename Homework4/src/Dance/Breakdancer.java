package Dance;

public class Breakdancer extends Dancer {
	
	Breakdancer(int age, String name) {
		super(age, name);
	}

	@Override
	public void dance() {
		
		System.out.println("I'm the coolest dancer in the room just 'cuz I carry a boombox around at all times");
		
	}
	
}
