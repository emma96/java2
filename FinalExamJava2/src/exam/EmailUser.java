package exam;

public class EmailUser extends User {
	
	private String emailAddress;

	//Constructor()
	public EmailUser(String firstName, String lastName, Address address, String emailAddress) {
		super(firstName, lastName, address);
		setEmailAddress(emailAddress);
	}
	
	//Getter and Setter
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	//toString()
	@Override
	public String toString() {
		return super.toString() + "EmailUser [emailAddress= " + emailAddress + "]";
	}

}
