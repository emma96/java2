package Dance;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		Dancer[] dancers = new Dancer[] {
				new Dancer(23, "Pancake"),
				new ElectricBoogieDancer(21, "BigBoogie"),
				new Breakdancer(22, "StreetCreep")
		};
		
		for(int i = 0; i < dancers.length; i++) {
			
			dancers[i].dance();
			
		}

	}

}
