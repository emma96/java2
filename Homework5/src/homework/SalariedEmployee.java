package homework;

public class SalariedEmployee extends Employee{

	protected double basicSalary;
	
	SalariedEmployee(String name, String ssn, double basicSalary) {
		super(name, ssn);
		
		this.basicSalary = basicSalary;
	}
	
	@Override
	public double salary() {
		return this.basicSalary * 8;
	}

	@Override
	public String toString() {
		return "SalariedEmployee [basicSalary=" + basicSalary + ", name= " + name + ", ssn= " + ssn + "]";
	}

}
