package ComparatorInterface;

import java.util.Comparator;

public class ScoreComparator implements Comparator<Student> {

	@Override
	public int compare(Student o1, Student o2) {
		if(o1.getScore() == o2.getScore()) {
			return 0;
		} else if (o1.getScore() > o2.getScore()) {
			return 1;
		} else {
			return -1;
		}
			
	}

}
