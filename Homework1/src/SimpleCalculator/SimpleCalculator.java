package SimpleCalculator;

public class SimpleCalculator {

	private double firstNumber;
	
	//Setter for firstNumber
	public void setFirstNumber(double firstNumber) {
		
		this.firstNumber = firstNumber;
		
	}
	
	//Getter for firstNumber
	public double getFirstNumber() {
		
		return firstNumber;
		
	}

	
	private double secondNumber;
	
	//Setter for secondNumber
	public void setSecondNumber(int secondNumber) {
		
		this.secondNumber = secondNumber;
		
	}
	
	//Getter for secondNumber
	public double getSecondNumber() {
		
		return secondNumber;
		
	}

	
	//Addition
	public double getAdditionResult() {
		
		double result = firstNumber + secondNumber;
		
		return result;
		
	}

	//Subtraction
	public double getSubtractionResult() {
		
		double result = firstNumber - secondNumber;
		
		return result;
		
	}

	//Multiplication
	public double getMultiplicationResult() {
		
		double result = firstNumber * secondNumber;
		
		return result;
		
	}

	//Division
	public double getDivisionResult() {
		
		double result = firstNumber / secondNumber;
		
		if(secondNumber == 0) {
			
			return 0;
			
		}
		
		return result;
		
	}

}
