package registration;

import java.util.List;

public class StudentWorkflow {
	
	public IRegistrationService registrationService ;
	
	public void insertData(List<Student> stdList) {
		
		for(Student std : stdList) {
			if(std instanceof MorningStudent) {
				MorningStudent mStd = (MorningStudent)std; //DownCasting
				registrationService = new RegistrationMorningStudent();
				registrationService.registerStudent(mStd);
			}
			else if(std instanceof EveningStudent)  {
				EveningStudent eStd = (EveningStudent) std;
				registrationService = new RegistrationEveningStudent();
				registrationService.registerStudent(eStd);
			}
		}
	}
}
