package ComparableInterface;

public class Apple implements Comparable<Apple>{
	
	private String variety;
	
	private Color color;
	
	private int weight;

	public String getVariety() {
		return variety;
	}

	public void setVariety(String variety) {
		this.variety = variety;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	@Override
	public int compareTo(Apple other) {
		
//		if(this.weight < other.weight) {
//			
//			return -1; //return negative number if this apple's weight is less than the other apple's weight
//			
//		}
//		if(this.weight == other.weight) {
//			
//			return 0; //return 0 if their weight are equal
//			
//		}
//		
//		return 1; //return 1 if this apple's weight is greater than the other apple's weight
		
//		however, this is not the best way to compare two int values with eachother...
		
//		we could write it like this... : 
		
//		return this.weight.compareTo(other.weight);
		
//		BUT since weight is a primitive type and not an object, the compareTo() method doesn't apply
		
/*		the primitive int has a corresponding Integer wrapper type, which provides the static method compare() that we can use
		to compare two int values   so.....   */ 
		
		return Integer.compare(this.weight, other.weight);
	}

	@Override
	public String toString() {
		return "Apple [variety=" + variety + ", color=" + color + ", weight=" + weight + "]";
	}

}
