
public class B extends Marks{
	
	private double subject1;
	private double subject2;
	private double subject3;
	private double subject4;
	
	public B(double subject1, double subject2, double subject3, double subject4) {
		
		this.subject1 = subject1;
		this.subject2 = subject2;
		this.subject3 = subject3;
		this.subject4 = subject4;
	}

	@Override
	public double getPercentage() {
		
		double percentageAverageB = (this.subject1 + this.subject2 + this.subject3 + this.subject4) / 4;
		
		return percentageAverageB;
	}

}
