package test;

public class Main {

	public static void main(String[] args) {
		
		Animal dog = new Dog("woof");
		
		dog.makeSound();
		
		Animal cat = new Cat("meow");
		
		cat.makeSound();
		
		Animal parrot = new Parrot("kakaw");
		
		parrot.makeSound();
		
	}

}
