package HashMap;

import java.util.HashMap;

public class Main {

	public static void main(String[] args) {
		
		//Create HashMap object
		
		HashMap<String, String> capitalCities = new HashMap<String, String>();
		
		//Add keys and values (Country, City)
	    capitalCities.put("England", "London");
	    capitalCities.put("Germany", "Berlin");
	    capitalCities.put("Norway", "Oslo");
	    capitalCities.put("USA", "Washington DC");
	    System.out.println(capitalCities);
	    
	    System.out.println();
	    
	    //get() refers to key
	    
	    System.out.println(capitalCities.get("England"));
	    
	    System.out.println();
	    
	    //remove() refers to key
	    
	    capitalCities.remove("England");
	    System.out.println(capitalCities);
	    
	    System.out.println();
	    
	    //clear() removes all items
	    
	    capitalCities.clear();
	    System.out.println(capitalCities);
	    
	    System.out.println();
	    
	    //size() finds out how many items there are
	    
	    System.out.println(capitalCities.size());
	    
	    System.out.println();
	    
	    //Loop through the HashMap
	    //keySet() if you want the keys
	    //values() if you want the values
		
		//Add keys and values (Country, City)
	    capitalCities.put("England", "London");
	    capitalCities.put("Germany", "Berlin");
	    capitalCities.put("Norway", "Oslo");
	    capitalCities.put("USA", "Washington DC");
	    
	    // Print keys
	    for (String i : capitalCities.keySet()) {
	      System.out.println(i);
	    }
	    
	    System.out.println();
	    
	    // Print values
	    for (String i : capitalCities.values()) {
	      System.out.println(i);
	    }
	    
	    System.out.println();
	    
	    // Print keys and values
	    for (String i : capitalCities.keySet()) {
	      System.out.println("key: " + i + " value: " + capitalCities.get(i));
	    }
	    
	    System.out.println();
	    
	    //Create a HashMap object called people that will store String keys and Integer values:

	    // Create a HashMap object called people
	    HashMap<String, Integer> people = new HashMap<String, Integer>();


	    // Add keys and values (Name, Age)
	    people.put("John", 32);
	    people.put("Steve", 30);
	    people.put("Angie", 33);

	    for (String i : people.keySet()) {
	      System.out.println("Name: " + i + ", Age: " + people.get(i));
	    }

	}

}
