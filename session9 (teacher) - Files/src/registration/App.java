package registration;

import java.util.ArrayList;
import java.util.List;

class Student {
	private String name;
	private double score;

	public Student(String name, double score) {
		setName(name);
		setScore(score);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		if (score >= 0.0) {
			this.score = score;
		}
		else {
			throw new NumberFormatException("the score needs to be more than 0.0");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		long temp;
		temp = Double.doubleToLongBits(score);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(score) != Double.doubleToLongBits(other.score))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", score=" + score + "]";
	}
	

}

class MorningStudent extends Student{
	
	private String morningId;
	
	public MorningStudent(String name, double score, String morningId) {
		super(name, score);
		setMorningId(morningId);
	}

	public String getMorningId() {
		return morningId;
	}

	public void setMorningId(String morningId) {
		if (morningId.startsWith("MOR")) {
			this.morningId = morningId;
		}
		else {
			throw new IllegalArgumentException("the id of the morning student"
					+ " needs to be started by MOR");
		}
	}
	
	
}


class EveningStudent extends Student{
	private String eveningId;

	public EveningStudent(String name, double score, String eveningId) {
		super(name, score);
		setEveningId(eveningId);
	}

	public String getEveningId() {
		return eveningId;
	}

	public void setEveningId(String eveningId) {
		if (eveningId.startsWith("EVE")) {
			this.eveningId = eveningId;
		}
		else {
			throw new IllegalArgumentException("the id of the evening student"
					+ " needs to be started by EVE");
		}
	}
	
	

}

public class App {

	public static void main(String[] args) {
		
		Student student1 = new MorningStudent("morning - A", 10, "MOR1");
		Student student2 = new MorningStudent("morning - B", 20, "MOR2");
		
		Student student3 = new EveningStudent("evening - C", 15, "EVE1");
		Student student4 = new EveningStudent("evening - D", 17, "EVE2");

		List<Student> studentList = new ArrayList<Student>();
		studentList.add(student1);
		studentList.add(student2);
		studentList.add(student3);
		studentList.add(student4);
		
		StudentWorkflow workFlow = new StudentWorkflow();
		workFlow.insertData(studentList);

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
