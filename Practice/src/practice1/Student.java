package practice1;

public class Student {

	int id;
	
	//Setter for id
	public void setId(int id) {
		
		this.id = id;
		
	}
	
	//Getter for ID 
	public int getId() {
		
		return this.id;
		
	}
	
	String name;

	//Setter for name
	public void setName(String name) {
		
		this.name = name;
		
	}
	
	//Getter for name
	public String getName() {
		
		return this.name;
		
	}
	
	int age;
	
	//Setter for age
	public void setAge(int age) {
		
		this.age = age;
		
	}

	
	//Getter for age
	public int getAge() {
		
		return this.age;
		
	}
}
