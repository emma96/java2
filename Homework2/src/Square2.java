
public class Square2 extends Shapes{

	@Override
	public double getArea(double length) {
		
		double area = length * length;
		
		return area;
	}

	@Override
	public double getPerimeter(double length) {
		
		double perimeter = length * 4;
		
		return perimeter;
	}
	
	

}
