package homework;

public class Computer extends Machine {
	
	@Override
	public void start() {
		
		System.out.println("Computer: power on");
		
	}
	
	public void playGames() {
		
		System.out.println("Starting new game on computer");
		
	}
}
