package ComparableInterface;

import java.util.ArrayList;
import java.util.Collections;

public class Main {

	public static void main(String[] args) {
		
		ArrayList<Integer> list = new ArrayList<>();
		
		list.add(13);
		list.add(16);
		list.add(30);
		list.add(3);
		
		System.out.println(list);
		
		Collections.sort(list);
		
		System.out.println("List after the use of Collections.sort() : " + list);
		
		System.out.println();
		
		ArrayList<String> stringList = new ArrayList<>();
		
		stringList.add("Alex");
		stringList.add("Emma");
		stringList.add("Mia");
		
		System.out.println(stringList);
		
		Collections.sort(stringList, Collections.reverseOrder());
		
		System.out.println("String list after sorting in reverse: " + stringList);
		
		System.out.println();
		
		Apple apple1 = new Apple();
		Apple apple2 = new Apple();
		Apple apple3 = new Apple();
		
		ArrayList<Apple> appleList = new ArrayList<>();
		
		apple1.setWeight(12);
		apple2.setWeight(10);
		apple3.setWeight(40);
		
		appleList.add(apple1);
		appleList.add(apple2);
		appleList.add(apple3);
		
		appleList.forEach(app -> System.out.println("before sorting by weight : " + app));
		System.out.println();
		
		Collections.sort(appleList);
		
		appleList.forEach(app -> System.out.println("after sorting by weight : " + app));
		System.out.println();

	}

}
