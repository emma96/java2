package Generics;

public class NumericFns<T extends Number> {
	
	T ob;
	
	NumericFns(T ob) {
		
		this.ob = ob;
		
	}
	
	//return square of a number
	
//	double square() {
//		
//		return ob * ob;
//		
//	}
// This multiplication doesn't work because it can only be applied to numeric values, not OBJECTS. 
// What happens if you pass in a String ? java knows this and it won't let you perform the operation.
// How can we ensure that what we pass in as our Generic is a numeric number?
// Generics provide bounded types where you can bound the type of your generic to something... EXTENDS NUMBER
	
	double square() {
		
		return ob.intValue() * ob.doubleValue();
		
	}

}
