package registration;

import java.io.IOException;

public class RegistrationMorningStudent implements IRegistrationService {

	@Override
	public void registerStudent(Student student) {
		System.out.println("this is morning " + student);

		try {
			FileUtils.fileWriter("morning", student);
		} catch (IOException e) {
			System.err.println("problem in writing the info" + e.getMessage());

		}
	}

}
