
public class Car {
	
	private String make;
	private String year;
	
	public Car(String make, String year) {
		setMake(make);
		setYear(year);
	}
	
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getYear() {
		return year;
	}
	
	public void setYear(String year) {
		if (year.startsWith("20") && year.length() == 4 && year.matches("^\\d+$")) {
			this.year = year;
		}
		else {
			throw new NumberFormatException("the year needs to be "
					+ "more that 2000 and in correct format");
		}
		
		//MVC => models as Employee, User, Car
		//MVVM => WPF
		//View => a layer with classes to show messages, exceptions, errors
		//Controller =>  the intersection between view and model that uses service layer
		
	}
	
}

class RegisterCar {
	
	public void carFunctions() {
		//do the workflow
		try {
			Car generatedCar = generateCar("Honda", "201r");
			registerNewCar(generatedCar);
		}
		catch(NumberFormatException exc) {
			//never ignore an exception
			//System.out.println("an error happened " + exc.getMessage());
		}
		catch(IllegalArgumentException exc) {
			//view => send a beautiful message to the user(browser, desktop, mobile)
			System.out.println("an error happened " + exc.getMessage());
		}
		
	}
	
	// I add / ** and enter 
	
	/**
	 * 
	 * @param make
	 * @param year
	 * @return car object
	 * @throws IllegalArgumentException when the year is not in a correct format
	 */
	public Car generateCar(String make, String year) throws IllegalArgumentException {
		return new Car(make, year);
	}
	
	public void registerNewCar(Car car) {
		System.out.println("the car is registered");
	}
	
}
