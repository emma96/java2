
public class Circle2 extends Shapes {

	@Override
	public double getArea(double length) {
		
		final double pi = 3.1416; //approx
		
		double area = pi * Math.pow(length, 2);
		
		return area;
		
	}

	@Override
	public double getPerimeter(double length) {
		
		final double pi = 3.1416; //approx
		
		double perimeter = 2 * pi * length;
		
		return perimeter;
	}

}
