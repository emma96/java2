package files;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class App {

	public static void main(String[] args) {
		
		try {
			
			bufferedWriterExample();
			printWriterExample();
			fileOutputStreamExample();
			dataOutputStreamExample();	
			filesExample();
		} catch (IOException e) 
		{
			
			System.out.println("an exception happened");
			
		}
		
		

	}
	
	
	private static void bufferedWriterExample() throws IOException {
		
		//bufferedWriter
		String str1 = "My sentence1";
		String str2 = "My sentence2";
		
		try(BufferedWriter writer = new BufferedWriter(new FileWriter("src/resource/testfile1"))) 
		{
			writer.write(str1);
			writer.write("\n");
			writer.write(str2);
			
		} catch (IOException e)
		{
			throw e;
		}
		
		//Since the teacher used try with resource feature, don't need to close the stream
		
		System.out.println("bufferedWriterExample is done");
	}
	
	//it is good if you have text, go with this approach
	private static void printWriterExample() throws IOException {
		try(FileWriter fileWriter = new FileWriter("src/resource/testfile2")){
			PrintWriter printWriter = new PrintWriter(fileWriter);
		    printWriter.printf("Product name is %s and its price is %d $", "iPhone", 1000);
		    printWriter.println("");
			printWriter.println("this is the example for printwriter");
			printWriter.print("this is the second line");
		}
		catch(IOException exc) {
			throw exc;
		}
		
		System.out.println("printWriterExample is done");
	}
	
	
	private static void fileOutputStreamExample() throws IOException {
		String str = "this is A string that I need to convert into byte !!!!";
		//base64 hashed !!! encryption
		//the password needs to be decrypted 
		//cibersecurity 
		try(FileOutputStream outputStream = new FileOutputStream("src/resource/testfile3")){
			byte[] strToByte = str.getBytes();
			outputStream.write(strToByte);
		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}
		
		System.out.println("fileOutputStreamExample is done");

	}
	
	private static void dataOutputStreamExample() throws IOException {
		String str = "my sentence";
		try(FileOutputStream fos = new FileOutputStream("src/resource/testfile4");){
			DataOutputStream outputStream = new DataOutputStream(new BufferedOutputStream(fos));
			outputStream.writeUTF(str);
		} catch (IOException e) {
			throw e;
		}
				
		System.out.println("dataOutputStreamExample is done");

	}
	
	private static void filesExample() throws IOException {
		String str = "my test";
		Path path = Paths.get("src/resource/testfile5");
		byte[] strToBytes = str.getBytes();
		
		Files.write(path, strToBytes);
		System.out.println("filesExample is done");

	}

}
