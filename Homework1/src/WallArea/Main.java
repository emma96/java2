package WallArea;

public class Main {

	public static void main(String[] args) {
		
		Wall wall = new Wall(5,4); //set width and height
		
		System.out.printf("area = %.1f\n", wall.getArea());
		
		System.out.println();
		
		wall.setHeight(-1.5); //change height value independently with a setter
		
		System.out.printf("width = %.1f\n", wall.getWidth());
		System.out.printf("height = %.1f\n", wall.getHeight());
		System.out.printf("area = %.1f\n", wall.getArea());
		
	}

}
