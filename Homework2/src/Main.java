class Circle1 {
	
	private double r;
	
	final private double pi = 3.1416; //approx
	
	public void setR(double r) {
		
		this.r = r;
		
	}

	public double getArea() {
		
		double area = pi * Math.pow(r, 2);
		
		return area;
		
	}
	
	public double getPerimeter() {
		
		double perimeter = 2 * pi * r;
		
		return perimeter;
		
	}

}

class Square1 {
	
	private double length;
	
	public void setLength(double length) {
		
		this.length = length;
		
	}

	public double getLength() {
		
		return length;
		
	}

	public double getPerimeter() {
		
		double perimeter = length * 4;
		
		return perimeter;
		
	}

	public double getArea() {
		
		double area = length * length;
		
		return area;
		
	}
}

// etc...



public class Main {

	public static void main(String[] args) {
		
		//Without abstraction
		
		Circle1 circle1 = new Circle1();
		
		circle1.setR(4.5);
		
		System.out.printf("Circle1 area = %.2f\n", circle1.getArea());
		System.out.printf("Circle1 perimeter = %.2f\n", circle1.getPerimeter());
		
		Square1 square1 = new Square1();
		
		square1.setLength(5.5);
		
		System.out.printf("Square1 area = %.2f\n", square1.getArea());
		System.out.printf("Square1 perimeter = %.2f\n", square1.getPerimeter());
		
		//With abstraction
		
		Circle2 circle2 = new Circle2();
		
		System.out.printf("Circle2 area = %.2f\n", circle2.getArea(4.5));
		System.out.printf("Circle2 perimeter = %.2f\n", circle2.getPerimeter(4.5));
		
		Square2 square2 = new Square2();
		
		System.out.printf("Square2 area = %.2f\n", square2.getArea(5.5));
		System.out.printf("Square2 perimeter = %.2f\n", square2.getPerimeter(5.5));	
		
		//Without abstraction, I notice that there's a lot of repetition in the methods and even with the variables.
		//Using abstraction, I can create a blueprint of methods that the other shape objects can refer too and have their own calculations.
	
		System.out.println();
		
		//Animals
		
		Cats cat = new Cats();
		
		System.out.println(cat.sound());
		
		Dogs dog = new Dogs();
		
		System.out.println(dog.sound());
		
		System.out.println();
		
		//Marks
		
		A studentA = new A(44, 55.5, 60);
		
		System.out.printf("Average grade of student A is : %.2f\n", studentA.getPercentage());
		
		B studentB = new B(90, 85.3, 89.5, 95);
		
		System.out.printf("Average grade of student B is : %.2f\n", studentB.getPercentage());
	}

}
