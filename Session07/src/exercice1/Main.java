package exercice1;

import java.util.ArrayList;
import java.util.Collections;

public class Main {

	public static void main(String[] args) {
		
		ArrayList<Students> studentsList = new ArrayList<>();
		
		studentsList.add(new Students(95.5, "Andrei"));
		studentsList.add(new Students(80.9, "Bob"));
		studentsList.add(new Students(85.0, "Someone"));
		
		studentsList.forEach(std -> System.out.println(std));
		
		System.out.println();
		
		Collections.sort(studentsList);
		
		System.out.println("After sorting: ");
		
		studentsList.forEach(std -> System.out.println(std));
		

		//But why is returning -1, 0, 1 sorting them...
	}

}
