package clone;

class Employee implements Cloneable{
	private String name;

	public Employee(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}



public class App {
	public static void main(String[] args) {
		Employee emp = new Employee("Reza");
		
		try {
			Employee emp2 = (Employee)emp.clone();
			System.out.println(emp2.getName());
			
			if(emp2.equals(emp)) {
				System.out.println("they are equal");

			}
			
		} catch (CloneNotSupportedException e) {
			
		}
	}
}
