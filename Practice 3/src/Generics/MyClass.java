package Generics;

import java.util.HashMap;

public class MyClass<T, V> {

	T ob;
	V ob2;
	
	MyClass(T ob, V ob2) {
		
		this.ob = ob;
		this.ob2 = ob2;
		
	}
	
	HashMap<T, V> hashMap = new HashMap<>();
	
	public void showType() {
		
		System.out.println(ob.getClass().getName());
		System.out.println(ob2.getClass().getName());

	}
	
}
