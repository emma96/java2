package homework;

public class ComissionEmployee extends Employee{

	protected double sales;
	
	protected double commission;
	
	ComissionEmployee(String name, String ssn, double sales, double commission) {
		super(name, ssn);
		
		this.sales = sales;
		
		this.commission = commission;
	}

	@Override
	public double salary() {
		return this.sales * this.commission;
	}

	@Override
	public String toString() {
		return "ComissionEmployee [sales=" + sales + ", commission=" + commission + ", name= " + name + ", ssn= " + ssn + "]";
	}

}
