package Person;

public class Main {

	public static void main(String[] args) {
		
		Person person = new Person();

		person.setFirstName(""); //set to empty
		person.setLastName(""); //set to empty
		person.setAge(10);
		
		System.out.printf("Full name is : %s\n", person.getFullName());
		System.out.printf("Teen = %s\n", person.isTeen());
		
		person.setFirstName("John"); //First name is changed to John
		person.setAge(18); //Age is changed to 18
		
		System.out.println();
		System.out.printf("Full name is : %s\n", person.getFullName());
		System.out.printf("Teen = %s\n", person.isTeen());
		
		person.setLastName("Smith"); //Last name set to Smith
		
		System.out.println();
		System.out.printf("Full name is : %s\n", person.getFullName());
	}

}
