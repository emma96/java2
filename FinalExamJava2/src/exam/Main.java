package exam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

//		Test -> sendMessage() working fine

//		EmailMessage email = new EmailMessage(new EmailUser("Frosty", "The Snowman", new Address("Sesame Street", 123), "frosty@gmail.com"), new EmailUser("Spider", "Man", new Address("Sesame Street", 456), "theRealSpiderMan@gmail.com"), "Hey Spiderman, this is an email message");
//		email.sendMessage(email);
//		
//		SmsMessage smsMessage = new SmsMessage(new SmsUser("Wonder", "Woman", new Address("Sesame Street", 364), "5143677733"), new SmsUser("Iron", "Man", new Address("Sesame Street", 987), "5149871818"), "Hey Iron Man, hope you're doing well. This is an sms message.");
//		smsMessage.sendMessage(smsMessage);

//		List<Message> listOfMessages = new ArrayList<Message>();
//
//		EmailMessage email = new EmailMessage(
//				new EmailUser("Frosty", "The Snowman", new Address("Sesame Street", 123), "frosty@gmail.com"),
//				new EmailUser("Spider", "Man", new Address("Sesame Street", 456), "theRealSpiderMan@gmail.com"),
//				"Hey Spiderman, this is an email message");
//
//		SmsMessage smsMessage = new SmsMessage(
//				new SmsUser("Wonder", "Woman", new Address("Sesame Street", 364), "5143677733"),
//				new SmsUser("Iron", "Man", new Address("Sesame Street", 987), "5149871818"),
//				"Hey Iron Man, hope you're doing well. This is an sms message.");
//
//		listOfMessages.add(email);
//		listOfMessages.add(smsMessage);

		// I can't seem to get it to work properly that way omg! COME BACK TO IT!
//		for(int i = 0; i < listOfMessages.size(); i++) {
//				try {
//					if(listOfMessages.get(i).validateMessage) {
//						listOfMessages.get(i).sendMessage();
//					} else {
//						System.out.println("validation not approved");
//					}
//				}
//				catch (IOException exc) {
//					System.out.println(exc.getMessage());
//				}
//			}

		// Teacher's solution:

		List<Message> listOfMessages = new ArrayList<Message>();

		listOfMessages.add(new EmailMessage(new EmailUser("Judy", "Foster", new Address("Main Street", 1), "ab@g.com"),
				new EmailUser("Betty", "Beans", new Address("second street", 2), "vr@g.com"), "This is one email"));

		listOfMessages.add(new SmsMessage(new SmsUser("Judy", "Foster", new Address("Main Street", 1), "1111111111"),
				new SmsUser("Betty", "Beans", new Address("second street", 2), "2222222222"), "This is one sms"));

		try {
			listOfMessages.forEach(mss -> {
				if (mss instanceof EmailMessage) {
					ISendInfo info = new SendEmail();
					if (info.validateMessage(mss.getSender(), mss.getReceiver(), mss.getBody())) {
						info.sendMessage(new Message(mss.getReceiver(), mss.getSender(), mss.getBody()));
					}
				} else if (mss instanceof SmsMessage) {
					ISendInfo info = new SendSms();
					if (info.validateMessage(mss.getSender(), mss.getReceiver(), mss.getBody())) {
						info.sendMessage(new Message(mss.getReceiver(), mss.getSender(), mss.getBody()));
					}
				}
			});
		} catch (Exception exc) {
			System.out.println(exc.getMessage() + " The messages cannot be sent");
		}

		System.out.println("done");

	}

}
