package practice1;

public class Main {

	public static void main(String[] args) {
		
		//First instance of the Student class
		Student mark = new Student();
		
		mark.age = 15;
		
		mark.id = 123;
		
		mark.name = "Mark";
		
		//Second instance of the Student class
		Student tom = new Student();
		
		tom.age = 14;
		
		tom.id = 1234;
		
		tom.name = "Tom";
		
		//Third instance of the Student object with getters and setters
		Student bob = new Student();
		
		bob.setName("Bob");
		bob.setAge(17);
		bob.setId(12345);
		
		//Usually, for getters and setters, instance variables would be private 
		
		//Print		
        System.out.println(mark.name + " is " + mark.age + " years old and his student ID is " + mark.id);
        System.out.println(tom.name + " is " + tom.age + " years old and his student ID is " + tom.id);
        System.out.println(bob.getName() + ", " + bob.getAge() + ", " +  bob.getId());

	}

}
