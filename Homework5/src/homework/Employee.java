package homework;

public abstract class Employee {
	
	protected String name;
	
	protected String ssn;
	
	Employee(String name, String ssn) {
		
		this.name = name;
		
		this.ssn = ssn;
		
	}
	
	public abstract double salary();

	@Override
	public String toString() {
		return "Employee [name=" + name + ", ssn=" + ssn + "]";
	}
	
}
