package ComparatorInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Student {
	
	private String name;
	private int score;
	
	public Student(String name, int score) {
		
		this.name = name;
		this.score = score;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", score=" + score + "]";
	}
	
}

class Employee {
	
	private String name;
	private double salary;
	
	public Employee(String name, double salary) {
		
		this.name = name;
		this.salary = salary;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", salary=" + salary + "]";
	}
	
}

public class Main {

	public static void main(String[] args) {
		
		List<Student> studentList = new ArrayList<Student>();
		
		studentList.add(new Student("TOTO", 6));
		studentList.add(new Student("POPO", 1));
		studentList.add(new Student("JOJO", 8));
		studentList.add(new Student("COCO", 4));

		studentList.forEach(std -> System.out.println(std));
		
		System.out.println();
		
		Collections.sort(studentList, new ScoreComparator());
		System.out.println("Sorting with score: ");
		studentList.forEach(std -> System.out.println(std));
		
		System.out.println();
		
		Collections.sort(studentList, new NameComparator());
		System.out.println("Sorting with name: ");
		studentList.forEach(std -> System.out.println(std));
		
		//With comparator, I can compare more than one thing (better than comparable because less logic in main method)
	}

}
