package SimpleCalculator;

public class Main {

	public static void main(String[] args) {
		
		SimpleCalculator calculator = new SimpleCalculator();
		
		calculator.setFirstNumber(5.0);
		calculator.setSecondNumber(4);
		
		System.out.printf("add= %.1f\n", calculator.getAdditionResult());
		System.out.printf("subtract=  %.1f\n", calculator.getSubtractionResult());
		
		calculator.setFirstNumber(5.25); //Change value of firstNumber
		calculator.setSecondNumber(0); //Change value of secondNumber
		
		System.out.printf("multiply= %.1f\n", calculator.getMultiplicationResult());
		System.out.printf("divide= %.1f\n", calculator.getDivisionResult());

	}

}
