package WallArea;

public class Wall {
	
	private double width;
	
	private double height;
	
	//no-argument constructor
//	public Wall() {}
	
	//constructor
	public Wall(double width, double height) {
		
		if(width < 0) {
			
			this.width = 0;
			
		}
		
		if(height < 0) {
			
			this.height = 0;
			
		}
		
		this.width = width;
		
		this.height = height;
		
	}

	//Getter for width
	public double getWidth() {
		
		return width;
		
	}

	//Getter for height
	public double getHeight() {
		
		return height;
		
	}

	//Setter for width
	public void setWidth(double width) {
		
		this.width = width;
		
		if(width < 0) {
			
			this.width = 0;
			
		}
		
	}

	//Setter for height
	public void setHeight(double height) {
		
		this.height = height;
		
		if(height < 0) {
			
			this.height = 0;
			
		}
		
	}

	//Getter for area
	public double getArea() {
		
		double area = height * width;
		
		return area;
		
	}
}
